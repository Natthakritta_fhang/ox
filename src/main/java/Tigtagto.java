
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Natthakritta
 */
public class Tigtagto {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        char[][] XO = new char[3][3];

        boolean results = true;
        int round = 1;

        System.out.println("Welcome to OX Game");
        for (int row = 0; row < XO.length; row++) {
            for (int column = 0; column < XO.length; column++) {
                System.out.print("- ");
            }
            System.out.println("");
        }

        while (results == true) {
            System.out.println("");
            if (round % 2 == 0) {
                System.out.println("Turn O");
            } else if (round % 2 == 1) {
                System.out.println("Turn X");
            }
            System.out.println("Please input row,col");
            int r = kb.nextInt();
            int c = kb.nextInt();

            for (int row = 1; row < XO.length + 1; row++) {
                for (int column = 1; column < XO.length + 1; column++) {
                    if (row == r && column == c) {
                        if (round % 2 == 0) {
                            XO[row - 1][column - 1] = 'O';
                        } else if (round % 2 == 1) {
                            XO[row - 1][column - 1] = 'X';
                        }
                    }

                }
            }

            for (int row = 0; row < XO.length; row++) {
                for (int column = 0; column < XO.length; column++) {
                    if (XO[row][column] == 'X') {
                        System.out.print("X ");
                    } else if (XO[row][column] == 'O') {
                        System.out.print("O ");
                    } else {
                        System.out.print("- ");
                    }
                }
                System.out.println("");
            }
            round++;
            //check X
            if (XO[0][0] == 'X' && XO[1][0] == 'X' && XO[2][0] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[0][1] == 'X' && XO[1][1] == 'X' && XO[2][1] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[0][2] == 'X' && XO[1][2] == 'X' && XO[2][2] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[0][0] == 'X' && XO[0][1] == 'X' && XO[0][2] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[1][0] == 'X' && XO[1][1] == 'X' && XO[1][2] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[2][0] == 'X' && XO[2][1] == 'X' && XO[2][2] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[0][2] == 'X' && XO[1][1] == 'X' && XO[2][0] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[0][0] == 'X' && XO[1][1] == 'X' && XO[2][2] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            }
            //check O
            if (XO[0][0] == 'O' && XO[1][0] == 'O' && XO[2][0] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[0][1] == 'O' && XO[1][1] == 'O' && XO[2][1] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[0][2] == 'O' && XO[1][2] == 'O' && XO[2][2] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[0][0] == 'O' && XO[0][1] == 'O' && XO[0][2] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[1][0] == 'O' && XO[1][1] == 'O' && XO[1][2] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[2][0] == 'O' && XO[2][1] == 'O' && XO[2][2] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[0][2] == 'O' && XO[1][1] == 'O' && XO[2][0] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[0][0] == 'O' && XO[1][1] == 'O' && XO[2][2] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            }
            else if (round == 10) {
                System.out.println(">>> Drew <<<");
                break;
            }

        }

    }
}
